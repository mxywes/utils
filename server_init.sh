#!/bin/bash
#install docker from stable, docker-compose from latest and git from apt

DOCKER_URL="https://get.docker.com"
DOCKER_COMPOSE_URL="https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)"
compose_bash_compl="https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose"
install_loc="/usr/local/bin/docker-compose"

apt -yy update &>/dev/null
apt -yy upgrade &>/dev/null
apt -yy dist-upgrade &>/dev/null
if ! command -v curl &> /dev/null
then
    echo "curl not found"
    apt -yy install curl &>/dev/null
fi
if ! command -v docker &> /dev/null
then
    echo installing docker
    curl -fsSL $DOCKER_URL | sh - 
fi
if ! command -v docker-compose &> /dev/null
then
    echo installing docker-compose
    curl -fsSL $DOCKER_COMPOSE_URL -o $install_loc
    chmod +x $install_loc
fi
if command -v bash &> /dev/null
then
    mkdir -p /etc/bash_completion.d/
    curl -fsSL $compose_bash_compl -o /etc/bash_completion.d/docker-compose
fi
if command -v systemctl &> /dev/null
then
    echo "found systemd"
    systemctl enable docker
fi
if ! command -v git &> /dev/null
then
    echo installing git
    apt -yy install git &>/dev/null
fi

printf "Installed the following: \n$(docker -v) \n$(docker-compose -v) \n$(git --version)\n"
